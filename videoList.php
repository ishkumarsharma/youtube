<?php 
include('connection.php');

$select = 'Select * from videos order by videoId asc limit 12';
$execute = mysql_query($select);

?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Youtube - VideoList</title>
	<link rel="shortcut icon" href="favicon.png">
	<link href="bootstrap.min.css" rel="stylesheet">
    <script src="jquery-3.1.1.min.js"></script>
	<script src="bootstrap.min.js"></script>
</head>
<body>
	<div class="middle-box text-center">
        <div>
            <h3>Video List</h3>
		</div>
		
		<div class="row">
			<div class="col-sm-12">
				<div class="table-responsive ">
					<table class="table table-striped table-bordered table-hover" >
						<tbody id="showVideos">
						<?php
							if(mysql_num_rows($execute) > 0){
								while($row = mysql_fetch_assoc($execute)) {
									$postID = $row['videoId'];
									echo '<tr><td><img src="'.$row['thumbnail'].'" title="Thumbnail"></td><td><h4>'.$row['title'].'</h4><br><h6>'.$row['description'].'</h6><br><h6><b>Search Term :</b> '.$row['searchTerm'].'</h6></td></tr>';
								}
								echo '<tr class="loader" id="'.$postID.'" style="display:none;"><td colspan="2"><img src="loading.gif"></td></tr>';
							}else{
								echo '<tr><td colspan="2">No Video</td></tr>';
							}						
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
    </div>
<script>
$(window).scroll(function() {
	var lastId = $('.loader').attr('id');
    if($(window).scrollTop() + $(window).height() == $(document).height() && lastId != 0) {
        //alert(lastId);
		$.ajax({
			type:'POST',
			url:'getAllData.php',
			data:'id='+lastId,
			beforeSend:function(){
				$('.loader').show();
			},
			success:function(html){
				$('.loader').remove();
				$('#showVideos').append(html);
			}
		});
    }
});
</script>
</body>
</html>