<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Youtube</title>
	<link rel="shortcut icon" href="favicon.png">
	<link href="bootstrap.min.css" rel="stylesheet">
    <script src="jquery-3.1.1.min.js"></script>
    <script src="bootstrap.min.js"></script>
</head>
<body>
	<div class="middle-box text-center">
        <div>
            <h3>Welcome to Youtube</h3>
			<a href="homePage.php" target="_blank" class="btn btn-primary">HOME PAGE</a>
			<a href="videoList.php" target="_blank" class="btn btn-primary">VIDEO LIST</a>
        </div>
    </div>
</body>
</html>
