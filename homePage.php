<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Youtube - HomePage</title>
	<link rel="shortcut icon" href="favicon.png">
	<link href="bootstrap.min.css" rel="stylesheet">
    <script src="jquery-3.1.1.min.js"></script>
	<script src="bootstrap.min.js"></script>
</head>
<body>
	<div class="middle-box text-center">
        <div>
            <h3>Search the Video</h3>
			<div class="form-group" style="display:inline-flex;width:70%;">
				<input type="text" class="form-control" placeholder="Search" id="searchField">
				<button class="btn btn-primary" onclick="searchVideo();" style="margin-left:20px;">Search</button>
			</div>
		</div>
		
		
		<div class="row">
			<div class="col-sm-12">
				<div class="table-responsive ">
					<table class="table table-striped table-bordered table-hover">
						<tbody id="showVideos">
							<!--  JS   -->
						</tbody>
					</table>
				</div>
			</div>
		</div>
    </div>
<script>
$("#searchField").keypress(function(e) {
    if(e.which == 13) {
        searchVideo();
    }
});

function searchVideo(){
	var value = $('#searchField').val();
	if (value == ''){
		alert('Please Fill Query');
	}else{
		$.ajax({
			type : 'POST',
			data : {'value':value},
			url : 'api.php',
			dataType : 'json',
			success : function(data){
				var c = data.items;
				var htm = '';
				if(c.length > 0){
					for(var i = 0; i < c.length; i++){
						htm += '<tr><td><iframe width="280" height="150" src="https://www.youtube.com/embed/'+c[i].id.videoId+'" frameborder="0" allowfullscreen></iframe></td><td><h4 style="float:left;">'+c[i].snippet.title+'</h4><br><h6 style="float:left;">'+c[i].snippet.description+'</h6></td></tr>';
					}
				}else{
					htm += '<tr><td colspan="2">No Result Found</td></tr>';
				}
				$('#showVideos').html('');
				$('#showVideos').html(htm);
			}
		});
	}
}
</script>
</body>
</html>